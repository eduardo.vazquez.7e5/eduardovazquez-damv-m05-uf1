﻿using System;
using ExercicisOptimitzacio;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        //Author: Eduardo Vázquez Espín
        //Date: January 17th 2022
        //Description: First Unitary Tests
        
        [Test]
        public static void PassingTestAdd() {
            Assert.AreEqual(42, Exercici1.Add(40,2));
        }
        
        [Test]
        public static void FailingTestAdd() {
            Assert.AreEqual(5, Exercici1.Add(2,2));
        }
        
        [Test]
        public static void PassingTestSub() {
            Assert.AreEqual(42, Exercici1.Subtract(40,-2));
        }        
        
        [Test]
        public static void FailingTestSub() {
            Assert.AreEqual(42, Exercici1.Subtract(40,2));
        }        
        
        [Test]
        public static void PassingTestMul() {
            Assert.AreEqual(42, Exercici1.Multiply(21,2));
        }        
        
        [Test]
        public static void FailingTestMul() {
            Assert.AreEqual(42, Exercici1.Multiply(40,2));
        }
        
        [Test]
        public static void PassingTestDiv() {
            Assert.AreEqual(42, Exercici1.Divide(168,4));
        }        
        
        [Test]
        public static void FailingTestDiv() {
            Assert.AreEqual(42, Exercici1.Divide(40,2));
        }
        
        [Test]
        public static void PassingTestLeap1() {
            Assert.AreEqual(false, LeapYear.IsLeapYear(2001));
        }        
        
        [Test]
        public static void PassingTestLeap2() {
            Assert.AreEqual(true, LeapYear.IsLeapYear(2004));
        }     
        
        [Test]
        public static void PassingTestLeap3() {
            Assert.AreEqual(false, LeapYear.IsLeapYear(2100));
        }     
        
        [Test]
        public static void PassingTestLeap4() {
            Assert.AreEqual(true, LeapYear.IsLeapYear(2000));
        }

        [Test]
        public static void PassingTestMinTen1()
        {
            int[] vector = {-587, 696, -632, 705, 938, -691, -661, 183, -302, 187};
            Assert.AreEqual(-691,Min10Values.MinOf10Values(vector));
        } 
        
        [Test]
        public static void FailingTestMinTen1()
        {
            int[] vector = {-587, 696, -632, 705, 938, -691, -661, 183, -302, 187};
            Assert.AreEqual(183,Min10Values.MinOf10Values(vector));
        }         
        
        [Test]
        public static void PassingTestMinTen2()
        {
            int[] vector = new int[0];
            Assert.AreEqual(0,Min10Values.MinOf10Values(vector));
        }         
        
        [Test]
        public static void PassingTestHowMany1() {
            Assert.AreEqual(365, HowMany.HowManyDays(2001));
        }        
        
        [Test]
        public static void PassingTestHowMany2() {
            Assert.AreEqual(366, HowMany.HowManyDays(2004));
        }     
        
        [Test]
        public static void PassingTestHowMany3() {
            Assert.AreEqual(365, HowMany.HowManyDays(2100));
        }     
        
        [Test]
        public static void PassingTestHowMany4() {
            Assert.AreEqual(366, HowMany.HowManyDays(2000));
        }

        [Test]
        public static void PassingTestDNI1()
        {
            Assert.AreEqual('M', DNI.DNILetter(47989735));
        }
        
        [Test]
        public static void PassingTestDNI2()
        {
            Assert.AreEqual('Z', DNI.DNILetter(12345678));
        }
    }
}