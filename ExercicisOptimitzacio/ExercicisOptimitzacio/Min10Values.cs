namespace ExercicisOptimitzacio
{
    public class Min10Values
    {
        public static int MinOf10Values(int[] vector)
        {
            //Author: Eduardo Vázquez Espín
            //Date: January 17th 2022
            //Description: Finds and returns the min of 10 values (actually any number)
            
            if (vector.Length > 0)
            {
                int min = vector[0];
                for(int i = 1; i< vector.Length; i++)
                    if (min > vector[i])
                        min = vector[i];
                return min;
            }

            return 0;
        }
    }
}