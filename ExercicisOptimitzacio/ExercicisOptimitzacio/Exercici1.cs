﻿namespace ExercicisOptimitzacio
{
    public class Exercici1
    {
        
        //Author: Eduardo Vázquez Espín
        //Date: January 17th 2022
        //Description: Basic Algebraic Operations
        
        public static int Add(int x, int y) {
            return x + y;
        }
        
        public static int Subtract(int x, int y) {
            return x - y;
        }
        
        public static int Multiply(int x, int y) {
            return x * y;
        }

        public static int Divide(int x, int y) {
            return x / y;
        }

        static void Main() {}
    }
}