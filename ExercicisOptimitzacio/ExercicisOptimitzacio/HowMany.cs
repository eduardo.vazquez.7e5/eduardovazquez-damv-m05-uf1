namespace ExercicisOptimitzacio
{
    public class HowMany
    {
        //Author: Eduardo Vázquez Espín
        //Date: January 17th 2022
        //Description: Returns how many days in a year (depends on LeapYear)
        
        public static int HowManyDays(int any)
        {
            if (LeapYear.IsLeapYear(any)) return 366;
            return 365;
        }
    }
}