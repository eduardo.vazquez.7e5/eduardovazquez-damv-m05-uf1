namespace ExercicisOptimitzacio
{
    public class LeapYear
    {
        //Author: Eduardo Vázquez Espín
        //Date: January 17th 2022
        //Description: Indicates with a boolean if input year is leap year
        
        public static bool IsLeapYear(int any)
        {
            return any % 4 == 0 && ( any % 100 != 0 || any % 400 == 0);
        }
    }
}