namespace ExercicisOptimitzacio
{
    public class DNI
    {
        public static char DNILetter(ulong n)
        {
            //Author: Eduardo Vázquez Espín
            //Date: January 17th 2022
            //Description: Returns the letter of a given DNI
            
            char[] letter =
            {
                'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C',
                'K', 'E'
            };
            return letter[n%23];
        }
    }
}