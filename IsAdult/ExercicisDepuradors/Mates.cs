//Author: Eduardo Vázquez Espín
//Date: 15th November
//Description: Corregir la funció Mates
/*
using System;

namespace ExercicisDepuradors
{
    static class Mates
    {

        static void Main()

        {

            var result = 0.0;

            for (int i = 0; i < 10000; i++)

            {

                if (result > 100)
                    result = Math.Sqrt(result);
                
                //if (result < 0)
                //    result += result * result; //El programa mai entra en aquest if 
                
                result += 20.2;

            }


            Console.WriteLine("el resultat és {0}", result );

        }
    }
}*/
// Quan i == 1000,
//      inicialment                                         Result == 111,56230589874906
//      com és més gran que 100 fa l'arrel quadrada i dóna  Result == 10,562305898749054
//      finalment li suma 20.2 i acaba valent               Result == 30,762305898749055

// Com ja hem comentat, sí que passa de 110 (en i==999 assoleix aquest valor per exemple)
// Mai sobrepassa 120 però